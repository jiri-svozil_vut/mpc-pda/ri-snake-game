import torch
import random
import numpy as np
from collections import deque
import json
import os

from game.snake import SnakeGameAI, Direction, Point
from model import Linear_QNet, QTrainer
from helper import plot

MAX_MEMORY = 100_000
BATCH_SIZE = 1000
LR = 0.001

LOAD_FROM_FILE = True


class Agent:

    def __init__(self):
        self.n_games = 0
        self.epsilon = 0
        self.gamma = 0.9  # discount rate
        self.memory = deque(maxlen=MAX_MEMORY)
        self.model = Linear_QNet(11, 256, 3)
        self.trainer = QTrainer(self.model, lr=LR, gamma=self.gamma)

    @staticmethod
    def get_state(game):
        head = game.snake[0]
        point_l = Point(head.x - 20, head.y)
        point_r = Point(head.x + 20, head.y)
        point_u = Point(head.x, head.y - 20)
        point_d = Point(head.x, head.y + 20)

        dir_l = game.direction == Direction.LEFT
        dir_r = game.direction == Direction.RIGHT
        dir_u = game.direction == Direction.UP
        dir_d = game.direction == Direction.DOWN

        state = [
            # Danger straight
            (dir_r and game.is_collision(point_r)) or
            (dir_l and game.is_collision(point_l)) or
            (dir_u and game.is_collision(point_u)) or
            (dir_d and game.is_collision(point_d)),

            # Danger right
            (dir_u and game.is_collision(point_r)) or
            (dir_d and game.is_collision(point_l)) or
            (dir_l and game.is_collision(point_u)) or
            (dir_r and game.is_collision(point_d)),

            # Danger left
            (dir_d and game.is_collision(point_r)) or
            (dir_u and game.is_collision(point_l)) or
            (dir_r and game.is_collision(point_u)) or
            (dir_l and game.is_collision(point_d)),

            # Move direction
            dir_l,
            dir_r,
            dir_u,
            dir_d,

            # Food location
            game.food.x < game.head.x,  # food left
            game.food.x > game.head.x,  # food right
            game.food.y < game.head.y,  # food up
            game.food.y > game.head.y  # food down
        ]
        return np.array(state, dtype=int)

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def train_long_memory(self):
        if len(self.memory) > BATCH_SIZE:
            mini_sample = random.sample(self.memory, BATCH_SIZE)  # list of tuples
        else:
            mini_sample = self.memory

        states, actions, rewards, next_states, dones = zip(*mini_sample)
        self.trainer.train_step(states, actions, rewards, next_states, dones)
        #for state, action, reward, next_state, done in mini_sample:
         #   self.trainer.train_step(state, action, reward, next_state, done)

    def train_short_memory(self, state, action, reward, next_state, done):
        self.trainer.train_step(state, action, reward, next_state, done)

    def get_action(self, state):
        # random moves: tradeoff exploration / explotation
        self.epsilon = 80 - self.n_games
        final_move = [0, 0, 0]

        if random.randint(0, 200) < self.epsilon:
            move = random.randint(0, 2)
            final_move[move] = 1
        else:
            state0 = torch.tensor(state, dtype=torch.float)
            prediction = self.model(state0)
            move = torch.argmax(prediction).item()

            final_move[move] = 1

        return final_move


def train():
    agent = Agent()

    try:
        plot_scores, plot_mean_scores, total_score, record, n_games = load_plot()
        agent.n_games = n_games
        agent.model.load()
        print("Loaded")

    except:
        plot_scores = []
        plot_mean_scores = []
        total_score = 0
        record = 0
        print("New model")


    game = SnakeGameAI()
    while True:
        try:
            state_old = agent.get_state(game)

            final_move = agent.get_action(state_old)

            reward, done, score = game.play_step(final_move)

            state_new = agent.get_state(game)

            agent.train_short_memory(state_old, final_move, reward, state_new, done)
            agent.remember(state_old, final_move, reward, state_new, done)

            if done:  # if game is over
                # train long memory, plot result

                game.reset()
                agent.n_games += 1
                agent.train_long_memory()

                if score > record:
                    record = score

                print('Game', agent.n_games, 'Score', score, 'Record:', record)

                plot_scores.append(score)
                total_score += score
                mean_score = total_score / agent.n_games
                plot_mean_scores.append(mean_score)

                plot(plot_scores, plot_mean_scores)

        except KeyboardInterrupt:
            break
    print("Saving...")
    agent.model.save()
    save_plot(plot_scores, plot_mean_scores, total_score, record, agent.n_games)


def save_plot(plot_scores, plot_mean_scores, total_score, record, n_games, file_name='metadata.json', path='model'):
    data_to_save = {
        "total_score": total_score,
        "record": record,
        "n_games": n_games,
        "plot_scores": plot_scores,
        "plot_mean_scores": plot_mean_scores
    }
    model_folder_path = path
    if not os.path.exists(model_folder_path):
        os.makedirs(model_folder_path)

    file_name = os.path.join(model_folder_path, file_name)

    with open(file_name, 'w') as file:
        json.dump(data_to_save, file)


def load_plot(file_name='metadata.json', path='model'):
    try:
        file_name = os.path.join(path, file_name)
        with open(file_name, 'r') as file:
            loaded_data = json.load(file)

        plot_scores = loaded_data["plot_scores"]
        plot_mean_scores = loaded_data["plot_mean_scores"]
        total_score = loaded_data["total_score"]
        record = loaded_data["record"]
        n_games = loaded_data["n_games"]

        return plot_scores, plot_mean_scores, total_score, record, n_games

    except Exception as e:
        print(e)
        raise e
